// Operators

import UIKit

//all basic math/logical/bitwise operators exist
var sum = 3 + 5

var doesntWork = sum = 3    // = does not return a value

//operators do not allow overflow (throw EXC_BAD_INSTRUCTION)

var i = Int.max
println("do I execute?")

//special operators allow overflow

i = i &+ 1

// string concatenation

var concat = "Hello " + "Me"

// modulus handles doubles

var remainder = 13.23 % 1.3

// null coalescing operator

var settingVal : Bool?
var defaultVal : Bool? = true

var coalescedVal = (settingVal ?? defaultVal)

println(coalescedVal!)


