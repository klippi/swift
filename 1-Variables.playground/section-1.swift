// Variables

import UIKit

//basic declaration
var str           = "Hello, playground"
var number : Int  = 42
let constantVal   = 13.0

//optionals
var doIExist: String? = "i exist!"

if doIExist != nil {
    
    println(doIExist)
    println(doIExist!)
}

if let potentiallyExists : String! = doIExist {
    
    println(potentiallyExists)
    println(potentiallyExists!)
}

//tuples
var polarisError = (1234, "Visit not found.")

println(polarisError.0)
println(polarisError.1)

// not allowed: polarisError = (431, 431)

var hubError = (code: 431, message: "No Reservation Slots Available")

println(hubError.code)
println(hubError.message)

let (hubCode, hubMessage) = hubError

println(hubCode)
println(hubError)

//unicode support at the compiler level

//alternative character sets
var 阮富仲通电话 = "A String with a name readable by over a billion people.";

//emojis!
var 🐷string = "🐸"
