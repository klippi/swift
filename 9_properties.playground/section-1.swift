// Playground - noun: a place where people can play

//Properties, much like c# can now control your access
struct propertiesDemoStruct
{
    var max = 100;
    var min = 1;
    var middle : Float
        {
        get
        {
            return (Float(max)-Float(min))/2;
        }
    }
}


var ourStruct = propertiesDemoStruct();

var ourMiddle = ourStruct.middle;


//Catching Property changes.

struct frustrating
{
    private var unsettableInt: Int = 61;
    private var settableInt: Int = 61;
    internal var setMe: Int = 666
        {
        willSet
        {
            unsettableInt = 999;
        }
        didSet
        {
            settableInt = 777;
        }
    }
    var Ints: Int
        {
        get
        {
            return settableInt;
        }
        set
        {
            unsettableInt = newValue;
            settableInt = newValue;
        }
    }
    var getSettableInt: Int
        {
        get
        {
            return settableInt;
        }
    }
    var getUnsettableInt: Int
        {
        get
        {
            return unsettableInt;
        }
    }
}

var myFrustratingFunc = frustrating();

myFrustratingFunc.Ints = 5;

myFrustratingFunc.getSettableInt;

myFrustratingFunc.getUnsettableInt;

myFrustratingFunc.setMe = 777;




//! Quick Note on Access Control;

//! There are only 3 acess control modifiers;
//! Private - only avalible within that FILE.
//! Public - Availble to everything in that module or a module that imports it.
//! internal - Availble to everything in that module.

//! so... this works.

var myNum = myFrustratingFunc.unsettableInt;










