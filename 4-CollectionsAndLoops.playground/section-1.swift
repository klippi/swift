// Collections And Control Flow
// Note:  Arrays and Dictionaries are value types

import UIKit

// ***** Arrays *****

//arrays are value types!
var array      : [String]      = ["item1", "item2"]
var otherArray : Array<String> = array

array[1] = "blarg"

println(otherArray)
println(array)

array += ["new item"]

//homogeneous
//array[1] = 123  won't work

//base class of string and int?
var arrayWithMultipleTypes = ["string", 42]

//convenience
var convenientArray = [Int](count: 3, repeatedValue: 1)

// ***** Dictionaries *****

var dict      = ["red" : 0xff000000, "green" : 0x00ff00]
var otherDict = dict

dict["blue"] = 0x0000ff

println(otherDict)

//dict["yellow"] = "yellow is dumb"

//keys must be "Hashable"

// ***** Control Flow *****

//for loop errata
for (index, item) in enumerate(array) {
    println(index)
    println(item)
}

for i in 0...5 {
    println(i)
}

for _ in 0...5 {
    print("!")
}

//switch doesn't fallthrough by default
var switchVal = 3

switch switchVal {
case 1, 2:
    println("?")
case 3:
    println("!")
    fallthrough
case 4...10:
    println(";")
default:
    println(".")
}



