// Strings
// Note:  Strings are value types

import Foundation

//var vs let
var testStr = "this is a string"
testStr += " more string!"

let constStr = "can't change me"
//constStr += " more string!"  // doesn't work

//string formatting (interpolation)
var numPeople = 4
var lastName  = "Elliott"
var partyString = "\(lastName), party of \(numPeople)"

//standardish functions
partyString.extend("asdf")
partyString.hasPrefix("Elliott")
partyString.rangeOfString("party")

countElements(partyString)

if partyString == testStr {
    println("here!")
} else {
    println("no here!")
}
