// Playground - noun: a place where people can play

import UIKit


func swapTwoInts(inout a: Int, inout b: Int) {
    let temp = a;
    a=b;
    b=temp;
}

var a = 2;
var b = 61;

swapTwoInts(&a,&b);

print(a);
print(b);

func swapTwoDoubls( inout a: Double, inout b: Double)
{
    let temp = a;
    a=b;
    b=temp;
}

var aD = 2.01;
var bD = 3.14;

swapTwoDoubls(&aD, &bD);

print(aD);
print(bD);


//! Or we could just write. (almost exatly like c#)
func swapTwoValues<T>(inout a: T, inout b: T)
{
    let temp = a;
    a=b;
    b=temp;
}

var Marek = "Marek";
var Joe = "Joe";

swapTwoValues(&Marek, &Joe);

print(Marek);
print(Joe);


func somethingWithAKey<KEY,T>(inout a: KEY, inout b: T)
{
    //! aka a map maybe? The key could be a string, and int or whatever else you wanted it to be.
    //! sorry, i'm not implementing a map right now... :)
}

var someKey = "Joe";
var someValue = 1234;


//! Unlike c# or C++ you do not initialize it with the values aka you don't:
//! somethingWithAKey<String,Int>(&someKey, &someValue);
somethingWithAKey(&someKey, &someValue);


//! What now? Protocols.
//! Protocols allow you to make requirements for the type of items that are allowed.

protocol Container {
    typealias ItemType;
    mutating func append(item: ItemType);
    var count: Int {get}
    subscript(i:Int) -> ItemType {get}
}

//! 1) requires an append function that takes the type of the item.
//! 2) requires a property that returns an integer called count
//! 3) requires a subscript[i] that returns an item type

struct OurStack<T>: Container {
    var items = [T]()
    mutating func push(item:T) {
        items.append(item);
    }
    mutating func pop() -> T {
        return items.removeLast();
    }
    mutating func append(item: T) {
        self.push(item);
    }
    var count: Int {
        return items.count;
    }
    subscript(i: Int) -> T {
        return items[i];
    }
    
}

//! Array already has all three requirements of our Container protocol, so the only extention we have to do to it is this.
extension Array: Container {
    //Extention functions if they where required.
}

var aNewStackOfInts = OurStack<Int>();

aNewStackOfInts.append(12345);
aNewStackOfInts.append(6789);
aNewStackOfInts.append(54342);

var popAnInt = aNewStackOfInts.pop();

var getAnInt = aNewStackOfInts[1];

var getCount = aNewStackOfInts.count;
