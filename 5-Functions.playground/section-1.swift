// Functions!

//basic syntax (parameters are constants by default, use var to override!)

func doThing(withInteger: Int) -> String {
    return "A thing has been done with \(withInteger)"
//    withInteger = 4; // won't work!
}

println(doThing(14))

//does this really return nothing?

func returnNothing() {
    
}

println(returnNothing())

//honestly, i love tuples because i'm stupid

func getHttpError() -> (error: Int, message: String) {
    return (404, "Resource Not Found")
}

var httpError = getHttpError()

println("Error \(httpError.error): \(httpError.message)")

//apple can't let go of param names and we really wish they would

func paramNamesLive(#withInteger : Int, #andString: String) {
    
}

paramNamesLive(withInteger: 42, andString: "asdf as asd f")

//oh lordy, default values

func whatNowDotNet(adjective : String = "awesome") -> String {
    return "Default parameters are \(adjective)"
}

println(whatNowDotNet())
println(whatNowDotNet(adjective: "super"))

//variadic parameters (syntactic convenience for passing an array)

func giveMeAllYourInts(numbers: Int...) -> Int {
    var sum = 0;
    
    for number in numbers {
        sum += number
    }
    
    return sum
}

println(giveMeAllYourInts(3, 2, 4, 12, 2, 4, 2))

//boxing value types

func iCanChangeMyParam(inout number : Int) {
    number = 5
}

var number = 3
iCanChangeMyParam(&number)  //not a real pointer like c or c++
println(number)

//functions are first class citizens

func printAString(methodToCall : (String) -> String, adjective: String) {
    println(methodToCall(adjective))
}

var javaAlsoSucks : (String) -> String = whatNowDotNet;
//Are you boasting about something that has been in c# from version 2.0? (we are on version 6 now for ref)

printAString(javaAlsoSucks, "cool")







