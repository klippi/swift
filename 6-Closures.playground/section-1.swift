// Closures!  aka anonymous methods
//  *are reference types

/* General form

{ ( <parameters> ) -> <return type> in
    <stuff>
}

*/

let restaurants = ["Meat Bowl", "Jason's Deli", "Wyld Eggz", "Monbowlian Grill", "That One Indian Place That is Ok"]

sorted(restaurants, { (s1: String, s2: String) -> Bool in
    return s1 > s2
})

//implied argument types/return type
sorted(restaurants, { s1, s2 in
    return s1 > s2
})

//implicit return b/c single expression
sorted(restaurants, { s1, s2 in s1 > s2})

//short hand argument names
sorted(restaurants, {$0 > $1})

//trailing closures (b/c it's the last parameter)
sorted(restaurants) { $0 > $1 }

//operator functions (not really closure related)
sorted(restaurants, >)

//love stuff like this
restaurants.map() { (var str) -> String in
    return str + " is a good place to eat"
}

//closures "capture" values
func makeIncrementor(amount: Int) -> () -> Int {    var runningTotal = 0
        func incrementor() -> Int {        runningTotal += amount        return runningTotal    }
        return incrementor}

var incrementByTen = makeIncrementor(10)
var increment = makeIncrementor(1)

println(incrementByTen)
println(increment)
println(increment)
