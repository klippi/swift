//Enumerations ... not just integers!

enum TableStatus {
    case Dirty      //not 0, Dirty is now its own value
    case Clean
    case Open
    case Seated
}

var tableStatus = TableStatus.Open

switch tableStatus {
case .Dirty:
    println("Clean me!")
case .Clean:
    println("Seat me!")
case .Open:
    println("I'm potentially redundant with both Clean and Dirty!")
case .Seated:
    println("So am I")
}

//associated values (never seen this)

enum EmployeeField {
    case Name(String, String)
    case Address(Int, String)
    case City(String)
}

var employeeName = EmployeeField.Name("Joe", "Elliott")

switch(employeeName) {
case let .Name(first, last):
    println("\(first) \(last)")
case let .Address(streetNum, streetName):
    println("\(streetNum) \(streetName)")
case let .City(cityName):
    println(cityName)
}

//raw values, i.e. how to line them up against integers

enum PolarisTableStatus : Int {
    case Dirty  = 0
    case Clean  = 1
    case Open   = 2
    case Seated = 3
    
    static let statusNames = [Dirty : "Dirty", Clean : "Clean", Open : "Open", Seated : "Seated"]
    
    //enum functions
    func getName() -> String? {
        return PolarisTableStatus.statusNames[self]
    }
    
    static func statusForName(nameToFind : String) -> PolarisTableStatus? {

        for (status, name) in PolarisTableStatus.statusNames {
            if(name == nameToFind) {
                return status
            }
        }
        
        return nil;
    }
}

//convert raw value -> enum
if let polarisTableStatus = PolarisTableStatus(rawValue: 2) {
    println(polarisTableStatus.getName()!)
    
    polarisTableStatus.rawValue
}

if let polarisTableStatus = PolarisTableStatus.statusForName("Seated") {
    println(polarisTableStatus.getName()!)
}

