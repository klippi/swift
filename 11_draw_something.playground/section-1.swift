// Playground - noun: a place where people can play

import Cocoa
import XCPlayground


class CustomView: NSView
{
    var color = NSColor.redColor();
    
    var curveColor = NSColor.whiteColor();
    
    var bezierPath = NSBezierPath();
    
    override init(frame: NSRect)
    {
        super.init(frame: frame);
        
        bezierPath.moveToPoint(NSPoint(x: 100 , y: 100));
        for i in 0...50
        {
            bezierPath.lineToPoint(NSPoint(x: 30 * (25-i), y: 30 * i));
            bezierPath.moveToPoint(NSPoint(x: 20 * (i), y: 10 ));
            bezierPath.moveToPoint(NSPoint(x: 10 * (i), y: 2 * ((i)/2)));
        }
    }
    
    convenience init (color: NSColor, frame: NSRect)
    {
        self.init(frame: frame);
        self.color = color;
    }

    required init(coder: NSCoder)
    {
        fatalError("has not been implemented");
    }
    override func drawRect(dirtyRect: NSRect)
    {
        color.setFill();
        NSRectFill(self.bounds);
        bezierPath.stroke();
        
    }
    
}

var view = CustomView(frame: NSRect(x: 0, y: 0, width: 300, height: 300));

var smallview = CustomView(frame: NSRect(x: 0, y:0, width: 100, height:100));

var blueView = CustomView(color: NSColor.blueColor(),
                          frame: NSRect(x: 0, y: 0, width: 300, height: 300));

XCPShowView("Large", view);
XCPShowView("Small", smallview);
XCPShowView("BlueView", blueView);

0...50
