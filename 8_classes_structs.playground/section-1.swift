// Playground - noun: a place where people can play

enum Tangibility {
    case not
    case probablyNot
    case maybe
    case probably
    case probablyForSure
    
}

import UIKit

var str = "Hello, playground"

//! Classes Vs Structures
//! Thigs Classes can do that Structs Cant
//! 1) Inheritance
//! 2) Type Casting (this is a no no on structs)
//! 3) Deinitializers
//! 4) Reference Counting (Structures are ALWAYS VALUE COPPIED (unless you use inout))
class MyFirstClass {
    var Name = "SomeName";
    let id = 1;
    
    //Initializer.
    init()
    {
        self.Name = "Marek";
    }
    
    init(newName: String)
    {
        self.Name = newName;
    }
}

struct myFirstStruct
{
    var name = "Marek";
    var age = 99;
    var address = "7463 N English Station Rd.";
    
    func ageDevidedByTwo() -> Float
    {
        return Float(age)/2;
    }

}

//! OKEY!
class MySecondClass: MyFirstClass {
    var tangability:Tangibility = Tangibility.probably;

    override init()
    {
        super.init();
    }
    
    init(myName: String)
    {
        super.init(newName: myName);
    }


}

/*
//! NOT OKEY! Classes cannot inherit from a struct as they don't have any inheritance... poor suckers.
class myThirdClass: myFirstStruct {
    var tangability:Tangibility = Tangibility.probablyNot;
}

//! NOT OKEY! Structs cannot inhearet from something.
struct myThirdStruct: myFirstStruct {
    var tangability:Tangibility = Tangibility.probablyNot;
}
*/


var mySecondClass = MySecondClass();

var canIHoldIt = mySecondClass.tangability;

var myStruct = myFirstStruct();

var halfAge = myStruct.ageDevidedByTwo();

var myClass = MyFirstClass();
var myName = myClass.Name;

